#!C:/Python27/python.exe
'''
__author__ = 'Jessica'
'''

from __future__ import print_function
import cgi


def html_top():
    print("""Content-type:text/html\n\n
            <!DOCTYPE html>
            <html lang = "en">
                <head>
                    <meta charset = "utf-8"/>
                    <title>Practicing stuff</title>
                </head>
                <body>""")

def html_tail():
    print("""</body>
            </html>""")


def get_data():
    form_data = cgi.FieldStorage()
    first_name = form_data.getvalue('lname')
    return first_name

if __name__ == "__main__":
    try:
        html_top()
        first_name = get_data()
        print("Hello %s" %(first_name))
        html_tail()
    except:
        cgi.print_exception()
